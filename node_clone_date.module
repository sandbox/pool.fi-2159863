<?php

/**
 * @file
 * Clone nodes into new ones with different dates from a date repeat form.
 */

/**
 * Implements hook_menu().
 */
function node_clone_date_menu() {
  $items['node/%node/clone_date'] = array(
    'access callback' => 'node_clone_date_access',
    'access arguments' => array(1),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('node_clone_date_repeat_form', 1),
    'title callback' => 'node_clone_date_action_link_title',
    'title arguments' => array(1),
    'weight' => 5,
    'file' => 'node_clone_date.pages.inc',
    'type' => MENU_LOCAL_ACTION,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
  );
  $items['admin/config/content/node_clone_date'] = array(
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('node_clone_date_settings'),
    'title' => 'Date clone module',
    'file' => 'node_clone_date.pages.inc',
  );
  return $items;
}

/**
 * Access callback for date cloning.
 */
function node_clone_date_access($node) {
  $module_settings = variable_get('node_clone_date_settings');
  if (!isset($module_settings[$node->type])) {
    return FALSE;
  }
  global $user;
  return user_access('clone node by date') || user_access('clone own nodes by date') && ($user->uid == $node->uid);
}

/**
 * Menu title callback.
 */
function node_clone_date_action_link_title($node) {
  $message = t('Clone this !type by date', array('!type' => drupal_strtolower(node_type_get_name($node))));
  // Alter action link message with hook.
  // @TODO: Use variables and variable translation instead.
  drupal_alter('node_clone_date_button_title', $message, $node);
  return $message;
}

/**
 * Implements hook_permission().
 */
function node_clone_date_permission() {
  return array(
    'clone node by date' => array('title' => t('Clone any node by date')),
    'clone own nodes by date' => array('title' => t('Clone own nodes by date.')),
  );
}

/**
 * Implements hook_node_insert().
 */
function node_clone_date_node_insert($node) {
  if (empty($node->node_clone_date)) {
    return;
  }
  // Save cloned nids into a static array. Horrible solution, needs
  // to be revisited.
  $cloned_nids = &drupal_static('node_clone_date_repeat_nids');
  if (!isset($cloned_nids)) {
    $cloned_nids = array();
  }
  $cloned_nids[$node->nid] = $node->nid;
}
