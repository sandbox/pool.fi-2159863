<?php
/**
 * @file
 * Hooks provided by the Node clone date repeat module.
 */

/**
 * Alter the action link text.
 *
 * @param string $message
 *   The current link text.
 * @param string $node
 *   The node to be cloned.
 */
function hook_node_clone_date_button_title(&$message, $node) {
  if ($node->type == 'page') {
    $message = t('Copy page');
  }
}

/**
 * Alter the node before cloning using node_clone.
 *
 * @param object $node
 *   Reference to the fully loaded node object being saved (the clone) that
 *   can be altered as needed.
 * @param array $context
 *   An array of context describing the clone operation. The context is
 *   generated from @code $form_state['node_clone_date']['context'] @endcode
 *   and can be modified by altering the form "node_clone_date_repeat_form".
 *
 * @see drupal_alter()
 * @see hook_form_FORM_ID_alter()
 */
function hook_node_clone_date_alter(&$node, &$context) {
  if ($node->type == 'page') {
    // Set cloned nodes to unpublished status.
    $node->status = 0;
  }
}
