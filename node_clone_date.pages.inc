<?php

/**
 * @file
 * Menu callbacks for the node_clone_date module.
 *
 * @todo use Batch API.
 * @todo confirmation form before clone.
 * @todo use clone_node_save instead of clone_action_clone.
 * @todo how to handle redirect after cloning?.
 */

/**
 * Module settings form.
 */
function node_clone_date_settings($form, &$form_state) {
  $module_settings = variable_get('node_clone_date_settings');
  $field_map = field_info_field_map();
  $date_field_map = array_filter($field_map, function ($field) {
    // Supported field types
    // @TODO: Does the module work with other types of date fields?
    // In that case, add them to this array.
    return in_array($field['type'], array('datetime'));
  });

  // Generate array of possible settings for all supported fields.
  $bundle_settings = array();
  foreach ($date_field_map as $field_name => $field) {
    if (isset($field['bundles']['node'])) {
      foreach ($field['bundles']['node'] as $bundle_name) {
        $field_info = field_info_instance('node', $field_name, $bundle_name);
        $bundle_settings[$bundle_name][$field_name] = $field_name . ' (' . $field_info['label'] . ')';
      }
    }
  }

  $form['hint'] = array(
    '#markup' => t('Select which content type(s) and respective date field you wish to enable the date repeat clone action for.'),
  );

  $form['node_clone_date_settings'] = array(
    '#tree' => TRUE,
    '#theme' => 'table',
    '#header' => array(
      t('Content type'),
      t('Machine name'),
      t('Repeat on field'),
      t('Load default date from source node'),
    ),
    '#rows' => array(),
  );

  $field_info_bundles = field_info_bundles('node');
  foreach ($bundle_settings as $bundle => $fields) {
    $form['node_clone_date_settings'][$bundle] = array(
      'enabled' => array(
        '#title' => $field_info_bundles[$bundle]['label'],
        '#type' => 'checkbox',
        '#default_value' => isset($module_settings[$bundle]),
      ),
      'machine_name' => array(
        '#markup' => $bundle,
      ),
      'date_field' => array(
        '#type' => 'select',
        '#options' => $fields,
        '#default_value' => isset($module_settings[$bundle]['field']) ? $module_settings[$bundle]['field'] : NULL,
      ),
      'default' => array(
        '#type' => 'checkbox',
        '#default_value' => !empty($module_settings[$bundle]['default']),
      ),
    );
    $form['node_clone_date_settings']['#rows'][] = array(
      array('data' => & $form['node_clone_date_settings'][$bundle]['enabled']),
      array('data' => & $form['node_clone_date_settings'][$bundle]['machine_name']),
      array('data' => & $form['node_clone_date_settings'][$bundle]['date_field']),
      array('data' => & $form['node_clone_date_settings'][$bundle]['default']),
    );
  }
  return system_settings_form($form);
}

/**
 * Validation callback for node_clone_date_settings().
 */
function node_clone_date_settings_validate(&$form, &$form_state) {
  // Clean up submitted data to preserve memory.
  $module_settings = &$form_state['values']['node_clone_date_settings'];
  foreach ($module_settings as $bundle => $settings) {
    if ($settings['enabled']) {
      $module_settings[$bundle] = array(
        'field' => $settings['date_field'],
        'default' => $settings['default'],
      );
    }
    else {
      unset($module_settings[$bundle]);
    }
  }
}

/**
 * Menu callback: Get a date repeat form for a node.
 * @TODO: Only allow on no-repeat fields with an end date (?).
 */
function node_clone_date_repeat_form($form, &$form_state, $node) {
  $module_settings = variable_get('node_clone_date_settings');
  if (!isset($module_settings[$node->type])) {
    drupal_set_message(t('Node clone date repeat is not set up for this node type. Click <a href="@config-link">here</a> to configure.', array('@config-link' => url('admin/config/content/node_clone_date'))), 'error');
    return;
  }
  $field_name = $module_settings[$node->type]['field'];
  $instance = field_info_instance('node', $field_name, $node->type);
  $field = field_info_field($field_name);
  // Check that field info exists.
  if (empty($field) || empty($instance)) {
    drupal_set_message(t('Field @field-name not found in this node type. Click <a href="@config-link">here</a> to re-configure.', array('@field-name' => $field_name, '@config-link' => url('admin/config/content/node_clone_date'))), 'error');
    return;
  }

  // Prepare element data.
  $items = $module_settings[$node->type]['default'] ? field_get_items('node', $node, $field_name) : array();
  $langcode = field_language('node', $node, $field_name);
  $parents = array();
  $form_state['node_clone_date'] = array(
    'field_name' => $field_name,
    'langcode' => $langcode,
    'context' => array(
      'module' => 'node_clone_date',
    ),
    'callback' => 'node_clone_date_finished',
  );
  // Form base.
  $form = array(
    '#parents' => $parents,
  );

  // Force repeat to form field.
  $field['settings']['repeat'] = 1;

  // Create date array.
  $field_form = field_default_form('node', $node, $field, $instance, $langcode, $items, $form, $form_state);
  $form[$field_name] = reset($field_form);

  // Set form field state.
  $field_state = array(
    'field' => $field,
    'instance' => $instance,
    // Should always be 1.
    'items_count' => count($items),
    'array_parents' => array(),
    'errors' => array(),
  );
  field_form_set_state($parents, $field_name, $langcode, $form_state, $field_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Clone'),
  );
  if (isset($field_form['field_order_date']['#weight'])) {
    $form['submit']['#weight'] = $field_form['field_order_date']['#weight'] + 1;
  }

  return $form;
}

/**
 * Submission callback for node_clone_date_repeat_form.
 */
function node_clone_date_repeat_form_submit($form, &$form_state) {
  $node = $form_state['complete form']['#entity'];
  $node->node_clone_date = TRUE;
  $field_name = $form_state['node_clone_date']['field_name'];
  $langcode = $form_state['node_clone_date']['langcode'];
  $clone_dates = $form_state['values'][$field_name][$langcode];
  // Create one node for each provided date.
  $node_count = 0;
  foreach ($clone_dates as $clone_date) {
    // Only use same fields as original node.
    $current_date = &$node->{$field_name}[$langcode][0];
    $current_date = array_intersect_key($clone_date, $current_date);
    $context = $form_state['node_clone_date']['context'];
    drupal_alter('node_clone_date', $node, $context);
    clone_action_clone($node, $context);
    $node_count++;
  }
  $callback = $form_state['node_clone_date']['callback'];
  if (function_exists($callback)) {
    $cloned_nids = &drupal_static('node_clone_date_repeat_nids');
    $context['new_nids'] = $cloned_nids;
    $context['node_count'] = $node_count;
    unset($cloned_nids);
    return $callback($context);
  }
  // Illegal callback, go to front.
  drupal_goto('<front>');
}

/**
 * Callback when cloning is complete.
 */
function node_clone_date_finished($context) {
  $node_count = $context['node_count'];
  if ($node_count == 1) {
    drupal_set_message(t('Node created.'));
    $nid = reset($context['new_nids']);
    drupal_goto('node/' . $nid . '/edit');
  }
  drupal_set_message(t('Created @node-count new nodes', array('@node-count' => $node_count)));
  drupal_goto('<front>');
}
